# Setup #

## Prequesits ##
[OpenCV 2.4.11](http://opencv.org/downloads.html)

Java 8

## Eclipse ##
Project -> Properties -> Java Build Path -> Add External JARs... -> <OpenCV install dir>/build/java/opencv-2411.jar

Project -> Properties -> Java Build Path -> Expand JRE System Library [...] -> Native library location -> Edit.. -> <OpenCV install dir>/build/java/x64 (32-bit: x86)

![setup_opencv_eclipes.png](https://bitbucket.org/repo/KK4a7p/images/20330192-setup_opencv_eclipes.png)