package at.itm15.immersive.book.debug;

import at.itm15.immersive.book.Settings;

public class DebugWriter {
	private static int count = 0;
	
	public static void reset() {
		count = 0;
	}
	
	/**
	 * Formats the output and prints it only if in debug mode (using sysout.println)
	 * @param msg The object to be output
	 */
	public static void println(Object msg) {
		if(!Settings.DEBUG) return;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < count; i++)sb.append(" ");
		sb.append(msg);
		System.out.println(sb.toString());
		count++;
	}
}
