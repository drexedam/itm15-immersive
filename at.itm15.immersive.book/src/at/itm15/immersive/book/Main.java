package at.itm15.immersive.book;

import at.itm15.immersive.book.cv.Analyzer;
import at.itm15.immersive.book.cv.InvalidSnapshotException;
import at.itm15.immersive.book.notification.MyNotifyReceiver;
import at.itm15.immersive.book.view.MainViewControler;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.InvalidParameterException;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.opencv.core.Core;

public class Main extends Application {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    private static String patternPath = "";

    public static void main(String[] args) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        patternPath = parseArgs(args);
        Settings.init();
        launch(args);

    }

    private static String parseArgs(String[] args) {
        if (args.length > 0 && (args[0].equals("-h") || args[0].equals("--help") || args[0].equals("/?"))) {
            System.out.println("---Usage---");
            System.out.println("<appname> <path-to-patterns>");
            System.exit(0);
        } else if (args.length > 0) {
            return args[0];
        }
        return null;
    }

    BorderPane mainPane;

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainViewControler.class.getResource("MainView.fxml"));
            mainPane = (BorderPane) loader.load();
            MainViewControler controller = loader.getController();
            controller.setStage(primaryStage);
            controller.startAnalyser(patternPath);
            Scene scene = new Scene(mainPane);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        //moved to controller
//        Task<Void> tsk = new Task<Void>() {
//
//            @Override
//            protected Void call() throws Exception {
//
//                Analyzer analyzer = null;
//                try {
//                    analyzer = new Analyzer(patternPath);
//
//                    Analyzer.addNotifyResever(new MyNotifyReceiver());
//                    while (!Thread.interrupted()) {
//                        analyzer.analyzeSnapshot();
//                        try {
//                            Thread.sleep(3000);
//                        } catch (InterruptedException e) {
//                            break;
//                        }
//                    }
//
//                } catch (InvalidSnapshotException | InvalidParameterException | FileNotFoundException e) {
//                    System.err.println(e.getMessage());
//                } catch (Exception e) {
//                    System.err.println("Unknown exception!");
//                    System.err.println(e.getMessage());
//                    e.printStackTrace();
//                } finally {
//                    if (analyzer != null) {
//                        analyzer.destroy();
//                    }
//                }
//
//                return null;
//            }
//        };
//        Thread t = new Thread(tsk);
//        t.start();
//        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
//
//            @Override
//            public void handle(WindowEvent event) {
//                tsk.cancel();
//            }
//        });
    }
}
