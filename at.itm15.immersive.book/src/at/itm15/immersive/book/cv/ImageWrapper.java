package at.itm15.immersive.book.cv;

import org.opencv.core.Mat;

public class ImageWrapper {
	private Mat image;
	private String name;
	
	public ImageWrapper(String name) {
		image = new Mat();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public Mat getImage() {
		return image;
	}
	
	public void setImag(Mat img) {
		image = img;
	}
}
