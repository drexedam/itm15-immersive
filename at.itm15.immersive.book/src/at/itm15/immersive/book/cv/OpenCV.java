package at.itm15.immersive.book.cv;

import java.io.ByteArrayInputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javafx.scene.image.Image;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.features2d.KeyPoint;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import at.itm15.immersive.book.Settings;
import at.itm15.immersive.book.debug.DebugWriter;

public class OpenCV {

    private static VideoCapture capture;

    /**
     * Releases a capture device and may create a new one.
     *
     * @param deviceID The id of the capture device. 0 for default device, <0
     * for only releasing the device.
     */
    public static void setCaptureDevice(int deviceID) {
        if (capture != null) {
            capture.release();
        }

        if (deviceID < 0) {
            return;
        }

        capture = new VideoCapture(0);
    }

    /**
     * Captures an image from the camera.
     *
     * @return Captured image
     * @throws IllegalStateException
     */
    public static Mat captureImage() throws IllegalStateException {
        if (capture == null) {
            throw new IllegalStateException("Camera not initialized!");
        }

        ImageWrapper wrappedImage = new ImageWrapper("");
        if (!capture.isOpened()) {
            capture.open(0);

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
//		capture.set(Highgui.CV_CAP_PROP_FRAME_WIDTH, 640);
//		capture.set(Highgui.CV_CAP_PROP_FRAME_HEIGHT, 480);
        capture.retrieve(wrappedImage.getImage());
        //capture.release();
        if (Settings.DEBUG) {
            Highgui.imwrite("captured.png", wrappedImage.getImage());
        }
        return wrappedImage.getImage();
    }

    public static Image getLiveFXImage() {
        MatOfByte byteMat = new MatOfByte();
        Highgui.imencode(".bmp", captureImage(), byteMat);
        return new Image(new ByteArrayInputStream(byteMat.toArray()));
    }
    
    public static void SaveImage(String filename, Mat mat){
        Highgui.imwrite(filename, mat);
    }

    private static double lastThreshold = 100;
    private static Mat lastImg;
    private static int match_method = Imgproc.TM_SQDIFF_NORMED;

    /**
     * Checks if templ is contained in img by using matches
     *
     * @param img The image to look in
     * @param templ The template to look for
     * @return If the template was found
     */
    public static boolean containsImage(Mat img, Mat templ) {
        return containsByMatch(img, templ);
    }

    /**
     * Searches for matches between two images by using features
     *
     * @param img The image to look in
     * @param templ The template to look for
     * @return The number of found matches
     */
    public static double getMatches(Mat img, Mat templ) {
        return containsByFeatures(img, templ);
    }

    private static double bestAvgSlopeDerivation = Double.MAX_VALUE;

    /**
     * Resets variables used when searching for matches Use this when the image
     * to look in changes
     */
    public static void newRun() {
        bestAvgSlopeDerivation = Double.MAX_VALUE;
        currentMinDist = Double.MAX_VALUE;
        currentGoodMatches = 0;
//		maxMatches = -1;

        DebugWriter.println("RESET");
    }

    private static double currentMinDist = Double.MAX_VALUE;
    private static int currentGoodMatches = 0;

    /**
     * Searches for matches between two given images
     *
     * @param img The image to look in
     * @param templ The pattern to look for
     * @return Number of good matches
     */
    private static double containsByFeatures(Mat img, Mat templ) {
        DebugWriter.reset();
        Mat img1 = new Mat();
        Mat img2 = new Mat();

        // Step 0 Convert to gray sacale
        Imgproc.cvtColor(img, img1, Imgproc.COLOR_RGB2GRAY);
        // preProcess(img, img1);
        Imgproc.cvtColor(templ, img2, Imgproc.COLOR_RGB2GRAY);

        // Step 1 find Keypoints
        FeatureDetector detector = FeatureDetector.create(FeatureDetector.SURF);
        MatOfKeyPoint keypoints1 = new MatOfKeyPoint();
        MatOfKeyPoint keypoints2 = new MatOfKeyPoint();

        detector.detect(img1, keypoints1);
        detector.detect(img2, keypoints2);

        DebugWriter.println("KEYPOINTS 1 DIM: " + keypoints1.size().height + "x" + keypoints1.size().width);
        DebugWriter.println("KEYPOINTS 2 DIM: " + keypoints2.size().height + "x" + keypoints2.size().width);

        // Step 2 Calculate descriptors
        DescriptorExtractor extractor = DescriptorExtractor
                .create(DescriptorExtractor.SURF);

        Mat descriptors1 = new Mat();
        Mat descriptors2 = new Mat();

        extractor.compute(img1, keypoints1, descriptors1);
        extractor.compute(img2, keypoints2, descriptors2);

        if (descriptors1.empty() || descriptors2.empty()) {
            return -1;
        }

		// Step 3 Matching descriptor vectors using FLANN matcher
        DescriptorMatcher matcher = DescriptorMatcher
                .create(DescriptorMatcher.FLANNBASED);
        MatOfDMatch matches = new MatOfDMatch();

        matcher.match(descriptors1, descriptors2, matches);

        MinMax mm = new MinMax();
        mm.maxDist = 0;
        mm.minDist = 100;

        // -- Quick calculation of max and min distances between keypoints
        List<DMatch> lst = matches.toList();

        lst.forEach(element -> {
            double dist = element.distance;
            if (dist < mm.minDist) {
                mm.minDist = dist;
            }
            if (dist > mm.maxDist) {
                mm.maxDist = dist;
            }
        });

        DebugWriter.println("MIN. DIST: " + mm.minDist);
        DebugWriter.println("MAX. DIST: " + mm.maxDist);

		// -- Draw only "good" matches (i.e. whose distance is less than
        // 2*min_dist,
        // -- or a small arbitary value ( 0.02 ) in the event that min_dist is
        // very
        // -- small)
        // -- PS.- radiusMatch can also be used here.
        List<DMatch> goodMatchesList = new LinkedList<DMatch>();
        DMatch[] matchesArr = matches.toArray();

        float ratio = 0.8f; // As in Lowe's paper; can be tuned
        for (int i = 0; i < matchesArr.length - 1; ++i) {
            if (matchesArr[i].distance <= ratio * matchesArr[i + 1].distance
                    && matchesArr[i].distance <= Math.max(2 * mm.minDist, 0.02)) {
                goodMatchesList.add(matchesArr[i]);
            }
        }

        double currentAvgSlopeDeviation = getAvgSlopeDeviation(goodMatchesList, keypoints1.toList(), keypoints2.toList());
        double diff = Math.abs(currentAvgSlopeDeviation - bestAvgSlopeDerivation);

        DebugWriter.println("GOOD MATCHES: " + goodMatchesList.size());
        DebugWriter.println("DIFF: " + diff);
        DebugWriter.println("AVG DIST: " + (mm.maxDist + mm.minDist) / 2);

        if (mm.minDist < currentMinDist) {
            DebugWriter.println("New best");
            DebugWriter.println("CUR AVG SLOPE DEV: " + currentAvgSlopeDeviation);
            bestAvgSlopeDerivation = currentAvgSlopeDeviation;
            currentMinDist = mm.minDist;
//			currentGoodMatches = goodMatchesList.size();
//			DebugWriter.println("CUR GOOD MATCHES: "+currentGoodMatches);
//			maxMatches = goodMatchesList.size();
        } else {
            DebugWriter.println("NO new best");
            DebugWriter.println("CUR AVG SLOPE DEV: " + currentAvgSlopeDeviation);
//			DebugWriter.println("CUR GOOD MATCHES: "+currentGoodMatches);
            DebugWriter.println("********************************");
            return Integer.MAX_VALUE;
        }

        MatOfDMatch goodMatches = new MatOfDMatch();
        goodMatches.fromList(goodMatchesList);

        if (Settings.DEBUG) {
            Mat imgMatches = new Mat();
            Features2d.drawMatches(img1, keypoints1, img2, keypoints2,
                    goodMatches, imgMatches, new Scalar(0, 0, 255), new Scalar(0, 0, 255),
                    new MatOfByte(), Features2d.NOT_DRAW_SINGLE_POINTS);

            Highgui.imwrite("new_result" + count++ + ".png", imgMatches);
        }
        DebugWriter.println("********************************");
        return (mm.maxDist + mm.minDist) / 2; //goodMatchesList.size();
    }

    /**
     * @deprecated Calculates the average deviation of all slopes of given sets
     * of keypoints
     * @param goodMatches Matches found between keypoints1 and keypoints2
     * @param keypoints1 The first set of keypoints
     * @param keypoints2 The second set of keypoints
     * @return Average deviation of all slopes between two keypoints
     */
    private static double getAvgSlopeDeviation(List<DMatch> goodMatches, List<KeyPoint> keypoints1, List<KeyPoint> keypoints2) {

        List<Double> slopes = new LinkedList<Double>();

        for (int i = 0; i < goodMatches.size(); i++) {
            DMatch currentMatch = goodMatches.get(i);

            KeyPoint kpt1 = keypoints1.get(currentMatch.queryIdx);
            KeyPoint kpt2 = keypoints2.get(currentMatch.trainIdx);

            double slope = (kpt1.pt.y - kpt2.pt.y) / (kpt1.pt.x - kpt2.pt.y);

            slopes.add(slope);

        }

        double medianSlope = getMedianSlope(slopes);
        double avgDeviation = 0;
        double totalDeviation = 0;
        for (int i = 0; i < goodMatches.size(); i++) {
            DMatch currentMatch = goodMatches.get(i);

            KeyPoint kpt1 = keypoints1.get(currentMatch.queryIdx);
            KeyPoint kpt2 = keypoints2.get(currentMatch.trainIdx);

            double slope = (kpt1.pt.y - kpt2.pt.y) / (kpt1.pt.x - kpt2.pt.y);

            totalDeviation += Math.abs(slope - medianSlope);
        }

        avgDeviation = totalDeviation / goodMatches.size();

        DebugWriter.println("AVG DEV: " + avgDeviation);

        return avgDeviation;

    }

    /**
     * Calculates the median slope for a list of slopes
     *
     * @param slopes All slopes to consider
     * @return Median value of the given list
     */
    private static double getMedianSlope(List<Double> slopes) {

        List<Double> sorted = slopes.stream()
                .sorted((v1, v2) -> Double.compare(v1, v2))
                .collect(Collectors.toList());

        double median = 0;

        if (sorted.size() % 2 == 0) {
            median = ((double) sorted.get(sorted.size() / 2) + (double) sorted.get(sorted.size() / 2 - 1)) / 2;
        } else {
            median = sorted.get(sorted.size() / 2);
        }

        return median;
    }

    static int count = 0;

    /**
     * Searches for matches by using Imgproc.matchTemplate and returns if the
     * pattern is contained by considering a threshold
     *
     * @param img The image to look in
     * @param templ The pattern to look for
     * @return If the pattern was found in the image
     */
    private static boolean containsByMatch(Mat img, Mat templ) {
        if (lastImg == null || !lastImg.equals(img)) {
            lastThreshold = 100;
        }

        double minminvalue = 7;

        int result_cols = img.cols() - templ.cols() + 1;
        int result_rows = img.rows() - templ.rows() + 1;
        Mat result = new Mat(result_rows, result_cols, CvType.CV_32FC1);

        Imgproc.matchTemplate(img, templ, result, match_method);

        MinMaxLocResult mmr = Core.minMaxLoc(result);

        minminvalue = mmr.minVal;

        DebugWriter.println("MinMinValue: " + minminvalue);

        if (minminvalue < Settings.THRESHOLD && minminvalue < lastThreshold) {
            lastThreshold = minminvalue;
            Point matchLoc = mmr.minLoc;
            Core.rectangle(img, matchLoc, new Point(matchLoc.x + templ.cols(),
                    matchLoc.y + templ.rows()), new Scalar(0, 255, 0, 0));
            if (Settings.DEBUG) {
                Highgui.imwrite("outputResult.jpg", img);
            }
            return true;
        }

        return false;
    }

    /**
     * Compares two image in order to get the number of differences. The
     * returned value is computed by using Core.absdiff on the images,
     * converting the result into a binary image by using
     * Imgproc.adaptiveThreshol and then counting white pixels
     *
     * @param snap1 First image
     * @param snap2 Second image
     * @return A number representing the difference between the two images
     */
    public static int getDiff(Mat snap1, Mat snap2) {
        if (Settings.DEBUG) {
            Highgui.imwrite("getDiffSnap1.jpg", snap1);
        }
        if (Settings.DEBUG) {
            Highgui.imwrite("getDiffSnap2.jpg", snap2);
        }

        Mat snap1Gray = new Mat();
        Imgproc.cvtColor(snap1, snap1Gray, Imgproc.COLOR_RGB2GRAY);
        Mat snap2Gray = new Mat();
        Imgproc.cvtColor(snap2, snap2Gray, Imgproc.COLOR_RGB2GRAY);

        Mat result = new Mat();
        Core.absdiff(snap1Gray, snap2Gray, result);

        Mat binary = new Mat();

        Imgproc.adaptiveThreshold(result, binary, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 7, 20);

        int diff = Core.countNonZero(binary);
        if (Settings.DEBUG) {
            Highgui.imwrite("diffBinary.jpg", binary);
        }
//		for(int i = 0; i < result.rows(); i++) {
//			for(int j = 0; j < result.cols(); j++) {
//				byte[] b = new byte[result.channels()];
//				
//				if(result.get(i, j, b) != 0)
//					diff++;
//			}
//		}
        System.out.println(result.total());
        System.out.println(diff);

        return diff;
    }
}
