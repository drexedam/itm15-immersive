package at.itm15.immersive.book.cv;

import org.opencv.core.Mat;

public class AnalyzationResult {
	private String patternName;
	private boolean found;
	private double minMatches = 100;
	private int maxMatches = 0;
	protected Mat snapshot;
	
	
	public double getMinMatches() {return minMatches;}
	public void setMinMatches(double value){minMatches = value;}
	
	public int getMaxMatches() {return maxMatches; }
	public void setMaxMatches(int value) {maxMatches = value; }
	
	
	protected AnalyzationResult() {
		
	}
	
	protected void setFound(boolean value) {
		found = value;
	}
	
	protected void setPatternName(String value) {
		patternName = value;
	}
	
	public AnalyzationResult(boolean found, String patternName) {
		this.found = found;
		this.patternName = patternName;
	}
	
	public boolean patternFound() {
		return found;
	}
	
	public String getName() {
		return patternName;
	}
}
