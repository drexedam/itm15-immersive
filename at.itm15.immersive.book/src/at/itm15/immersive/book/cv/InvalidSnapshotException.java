package at.itm15.immersive.book.cv;

public class InvalidSnapshotException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4981204624639735835L;

	public InvalidSnapshotException(String msg) {
		super(msg);
	}
}
