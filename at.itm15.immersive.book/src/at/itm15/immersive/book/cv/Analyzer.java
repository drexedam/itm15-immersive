package at.itm15.immersive.book.cv;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import at.itm15.immersive.book.Settings;
import at.itm15.immersive.book.debug.DebugWriter;
import at.itm15.immersive.book.notification.INotifyResever;
import at.itm15.immersive.book.notification.NotificationArgs;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Analyzer {

        private ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private List<ImageWrapper> patterns;
	private static List<INotifyResever<AnalyzationResult>> receivers = new LinkedList<INotifyResever<AnalyzationResult>>();

	public Analyzer(String patternPath) throws InvalidParameterException,
			FileNotFoundException,
			InterruptedException {

		patterns = new LinkedList<ImageWrapper>();

		loadPatterns(patternPath);

		OpenCV.setCaptureDevice(Settings.CAMERA_ID);
	}

	/**
	 * Register a receiver to be notified after analyzation
	 * @param receiver Receives a notification
	 */
	public static void addNotifyResever(INotifyResever<AnalyzationResult> receiver) {
		receivers.add(receiver);
	}

	/**
	 * Notifies all registered NotifyReceivers
	 * @param result A result of the executed analyzation
	 */
	private void notifyReceivers(AnalyzationResult result) {
		for (INotifyResever<AnalyzationResult> receiver : receivers) {
			receiver.notifyMe(new NotificationArgs<AnalyzationResult>(result));
		}
	}

	/**
	 * Loads all patterns from a given path
	 * @param patternPath The path to look in for the patterns
	 * @throws InvalidParameterException
	 * @throws FileNotFoundException
	 */
	public boolean loadPatterns(String patternPath)
			throws InvalidParameterException, FileNotFoundException, InterruptedException {
		File patternFolder = new File(patternPath);

		if (!patternFolder.isDirectory()) {
			//throw new InvalidParameterException(patternPath
			//		+ " is not a directory");
			return false;
		}
                readWriteLock.writeLock().lock();
            try {
                File[] patternFiles = patternFolder.listFiles();
                if (patternFiles == null || patternFiles.length == 0) {
                    //	throw new FileNotFoundException("No pattern file found in "
                    //			+ patternPath);
                    return false;
                }
                
                for (File f : patternFiles) {
                    if (f.getName().endsWith(".png") || f.getName().endsWith(".jpg")) {
                        ImageWrapper iw = new ImageWrapper(f.getName());
                        
                        Mat mat = Highgui.imread(f.getPath(), 1);
                        
//				if(Settings.ALGORITHM == Algorithm.FeatureDetection)
//					mat = OpenCV.preProcess(mat); // Do some gray scale conversion and thresholding
                        
                        iw.setImag(mat);
                        patterns.add(iw);
                    }
                }
            } finally {
                readWriteLock.writeLock().unlock();
            }
		return patterns.size() > 0;
	}

	/**
	 * Searches for patterns in snapshots with a given algorithm
	 * @throws InvalidSnapshotException
	 */
	public void analyzeSnapshot() throws InvalidSnapshotException, InterruptedException {

		Function<Mat, AnalyzationResult> func = null;
                readWriteLock.readLock().lock();
            try {
                switch (Settings.ALGORITHM) {
                    case FeatureDetection:
                        func = new Function<Mat, AnalyzationResult>() {
                            
                            @Override
                            public AnalyzationResult apply(Mat snapshot) {
                                AnalyzationResult result = new AnalyzationResult(false, "");
                                result.snapshot = snapshot;
                                // Do some gray scale conversion and thresholding
                                //result.snapshot = OpenCV.preProcess(result.snapshot);
                                OpenCV.newRun();
                                
                                patterns.forEach(pattern -> {
                                    double matches = OpenCV.getMatches(pattern.getImage(),
                                            result.snapshot);
                                    if (matches < result.getMinMatches()){
//								&& matches >= Settings.MIN_MATCHES) {
                                        result.setFound(true);
                                        result.setPatternName(pattern.getName());
                                        result.setMinMatches(matches);
                                        DebugWriter.println("Best match: "
                                                + result.getName());
                                    }
                                });
                                
                                return result;
                            }
                        };
                        break;
                        
                    case Matching:
                    default:
                        func = new Function<Mat, AnalyzationResult>() {
                            
                            @Override
                            public AnalyzationResult apply(Mat snapshot) {
                                AnalyzationResult result = new AnalyzationResult(false, "");
                                
                                patterns.forEach(pattern -> {
                                    if (OpenCV.containsImage(snapshot, pattern.getImage())) {
                                        result.setFound(true);
                                        result.setPatternName(pattern.getName());
                                        DebugWriter.println("Found: " + result.getName());
                                    }
                                });
                                
                                return result;
                            }
                        };
                        break;
                }
                
                analyzeSnapshot(func);
            } finally {
                readWriteLock.readLock().unlock();
            }
                
	}

	/**
	 * Does the real analyzation
	 * @param algo The algorithm to be used
	 * @throws InvalidSnapshotException
	 */
	private void analyzeSnapshot(Function<Mat, AnalyzationResult> algo)
			throws InvalidSnapshotException {

		if(!stableSnapshot()) {
			notifyReceivers(new AnalyzationResult(false, "UNSTABLE"));
			return;
		}
		
		Mat snapshot = OpenCV.captureImage();

		if (Settings.DEBUG)
			Highgui.imwrite("captured.jpg", snapshot);

		if (snapshot == null)
			throw new InvalidSnapshotException(
					"Could not capture image from camera");

		AnalyzationResult result = algo.apply(snapshot);

		notifyReceivers(result);

	}

	/**
	 * Analyzes two snapshots to see if there is movement within the pictures
	 * @return True if there is hardly any movement false else
	 */
	private boolean stableSnapshot() {
		int count = 0;
		int dif = 100; // TODO Increase -> less motion detected but maybe necessary
		do {
			Mat snap1 = OpenCV.captureImage();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				break;
			}
			Mat snap2 = OpenCV.captureImage();
			
			dif = OpenCV.getDiff(snap1, snap2);
			
		}while(count < 15 && dif > 100);
		
		return count < 15;
	}

	public void destroy() {
		OpenCV.setCaptureDevice(-1);
	}
}
