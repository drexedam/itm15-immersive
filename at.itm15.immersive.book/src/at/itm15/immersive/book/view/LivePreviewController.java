/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.itm15.immersive.book.view;

import at.itm15.immersive.book.cv.OpenCV;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author ge0rgi
 */
public class LivePreviewController implements Initializable {

    @FXML
    private ImageView imageView;
    @FXML
    private BorderPane LivePreview;
    private Stage stage;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        
    }
    
    public void startLiveCapture(Stage stage2close){
        //does not work inside intialise because of thread pool
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        stage2close.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                scheduler.shutdownNow();
            }
        });
        scheduler.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                final Image img = OpenCV.getLiveFXImage();
                Platform.runLater(() -> {
                    imageView.setFitWidth(stage2close.getWidth());
                    imageView.setFitHeight(stage2close.getHeight());
                    imageView.setImage(img);
                });
            }
        }, 0, 1, TimeUnit.SECONDS);
    }
    
    @FXML
    private void saveImage(){
        FileChooser saveDialog = new FileChooser();
        saveDialog.setInitialDirectory(new File(System.getProperty("user.dir")));
        saveDialog.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG Image", "*.png"));
        File f = saveDialog.showSaveDialog(stage);
        if(f == null) return;
        OpenCV.SaveImage(f.getAbsolutePath(), OpenCV.captureImage());
        
        
        
    }
    
    public void setStage(Stage s){
        stage = s;
    }
    
}
