/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.itm15.immersive.book.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author ge0rgi
 */
public class TableEntry {
    
    private StringProperty pattern;
    private StringProperty content;
    
    public TableEntry(String pattern, String content){
        this.pattern = new SimpleStringProperty(pattern);
        this.content = new SimpleStringProperty(content);
    }
    
    public StringProperty patternProperty(){
        if (pattern == null){
            pattern = new SimpleStringProperty(this, "pattern");
        }
        return pattern;
    }
    
    public StringProperty contentProperty(){
        if (content == null){
            content = new SimpleStringProperty(this, "content");
        }
        return content;
    }
    
    public void setPattern(String value){
        pattern.set(value);
    }
    
    public void setContent(String value){
        content.set(value);
    }
    
    public String getPattern() {
        return pattern.get();
    }
    
   public String getContent() {
       return content.get();
   }
}
