/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.itm15.immersive.book.view;

import at.itm15.immersive.book.Settings;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author ge0rgi
 */
public class ConfiguratorController implements Initializable {

    @FXML
    private ImageView image;
    @FXML
    private TableView<TableEntry> table;
    @FXML
    private TableColumn<TableEntry, String> patternCol;
    @FXML
    private TableColumn<TableEntry, String> contentCol;
    private Stage stage;
    private Map<String, Image> images = new HashMap<>();
    private ObservableList<TableEntry> tableData;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //needed for update notification to work
        tableData= FXCollections.observableArrayList(new Callback<TableEntry, Observable[]>() {

            @Override
            public Observable[] call(TableEntry param) {
                return new Observable[] {param.patternProperty(), param.contentProperty()};
            }
        });
        contentCol.setCellFactory(TextFieldTableCell.forTableColumn());
        table.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TableEntry>() {

            @Override
            public void changed(ObservableValue<? extends TableEntry> observable, TableEntry oldValue, TableEntry newValue) {
                if(newValue != null){
                    Image img = images.get(newValue.getPattern());
                    if(img != null){
                        image.setImage(img);
                    }
                }
            }
        });
        tableData.addListener(new ListChangeListener<TableEntry>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends TableEntry> c) {
                //must be called before inspecting the change
                c.next();
                if(c.wasUpdated()){
                    //only one change at a time is possible
                    TableEntry changeEntry = tableData.get(c.getFrom());
                    Settings.PATTERNS.put(changeEntry.getPattern(), changeEntry.getContent());
                }
            }
        });
    }
    
    public void fillFromSettings() throws MalformedURLException {
        Map<String, String> patterns = Settings.PATTERNS;
        Set<String> keys = patterns.keySet();
        Iterator<String> keyItr =  keys.iterator();
        while(keyItr.hasNext()){
            String key = keyItr.next();
            String content = patterns.get(key);
            TableEntry entry = new TableEntry(key, content);
            tableData.add(entry);
            
        }
        table.setItems(tableData);
        //patternCol.setCellValueFactory(new PropertyValueFactory<TableEntry, String>("pattern"));
        //contentCol.setCellValueFactory(new PropertyValueFactory<TableEntry, String>("content"));
        File patternsPath = new File(Settings.PATTERN_PATH);
        for(File f: patternsPath.listFiles()){
            Image img = new Image(f.toURI().toURL().toString());
            String name = f.getName().split("\\.")[0];
            images.put(name, img);
            if(!Settings.PATTERNS.containsKey(name)){
                Settings.PATTERNS.put(name, "");
                table.getItems().add(new TableEntry(name, ""));
            }
        }
    }
    
    @FXML
    private void onAddVideo(){
        TableEntry entry = table.getSelectionModel().getSelectedItem();
        FileChooser dialog = new FileChooser();
        dialog.setTitle("Choose a video");
        File f = dialog.showOpenDialog(stage);
        if(f != null && f.exists()){
            entry.setContent(f.getAbsolutePath());
            //Settings.PATTERNS.put(entry.getPattern(), entry.getContent());
        }
        
    }

    /**
     * @return the stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * @param stage the stage to set
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
}
