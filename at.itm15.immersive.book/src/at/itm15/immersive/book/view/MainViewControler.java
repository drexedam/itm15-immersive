package at.itm15.immersive.book.view;

import at.itm15.immersive.book.Settings;
import at.itm15.immersive.book.cv.AnalyzationResult;
import at.itm15.immersive.book.cv.Analyzer;
import at.itm15.immersive.book.cv.InvalidSnapshotException;
import at.itm15.immersive.book.notification.INotifyResever;
import at.itm15.immersive.book.notification.MyNotifyReceiver;
import at.itm15.immersive.book.notification.NotificationArgs;
import com.sun.javafx.scene.SceneUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class MainViewControler implements INotifyResever<AnalyzationResult>, Initializable {

//	@FXML
//	private Label lbl1;
    @FXML
    BorderPane borderPane;
    private WebView browser;
    private MediaView mediaView;
    private String lastDetection = "";
    private int detectionCounter = 0;
    private boolean isMediaPlaying; //if false browser is shown
    private Stage mainStage;
    @FXML
    private ProgressBar progress;
    private String content;
    private Analyzer analyser;

    public MainViewControler() {
        Analyzer.addNotifyResever(this);
        browser = new WebView();
        mediaView = new MediaView();
        mediaView.setPreserveRatio(true);
    }

    @Override
    public void notifyMe(NotificationArgs<AnalyzationResult> args) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {

                // GUI changes here (notifyMe called from a different thread!)
                if (args.getArgs().patternFound()) {
                    String pattenName = args.getArgs().getName().split("\\.")[0];
                    progress.visibleProperty().set(!pattenName.equals(content));
                    if (!pattenName.equals(lastDetection) && Settings.PATTERNS.containsKey(pattenName)) {
                        lastDetection = pattenName;
                        detectionCounter = 1;
                    } else if (pattenName.equals(lastDetection)) {
                        detectionCounter++;
                        if (detectionCounter == Settings.PATTERN_REPEAT) {
                            showContent(pattenName);
                        }

                    } else if (!Settings.PATTERNS.containsKey(pattenName)) {
                        System.out.println("Unkwnown pattern");
                    }
                } else {
                    System.out.println("Pattern not found");
                }
                //lbl1.setText(args.getArgs().patternFound()+" "+args.getArgs().getName());

            }
        });
    }

    public void showVideo(String path) {
        if (isMediaPlaying) {
            mediaView.getMediaPlayer().stop();
        }
        Media media = new Media(path);
        MediaPlayer player = new MediaPlayer(media);
        player.setAutoPlay(true);
        mediaView.setMediaPlayer(player);
        mediaView.setFitWidth(mainStage.getWidth());
        mediaView.setFitHeight(mainStage.getHeight());
        borderPane.setCenter(mediaView);
        isMediaPlaying = true;

    }

    private void showBrowser(String path) {
        if (isMediaPlaying) {
            mediaView.getMediaPlayer().stop();
            isMediaPlaying = false;
        }
        borderPane.setCenter(browser);
        browser.getEngine().load(path);
    }

    public void setStage(Stage primary) {
        mainStage = primary;
    }

    private void showContent(String pattern) {
        progress.visibleProperty().set(false);
        content = pattern;
        String content4pattern = Settings.PATTERNS.get(pattern);
        if (content4pattern.isEmpty()) {
            return;
        }
        if (content4pattern.startsWith("http")) {
            showBrowser(content4pattern);
        } else {
            showVideo(new File(content4pattern).toURI().toString());
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void showLive() {
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader();
        try {
            BorderPane bp = loader.load(MainViewControler.class.getResourceAsStream("LivePreview.fxml"));
            LivePreviewController controller = loader.getController();
            controller.setStage(stage);
            stage.setScene(new Scene(bp));
            stage.setTitle("Live Preview");
            stage.show();
            controller.startLiveCapture(stage);
        } catch (IOException ex) {
            System.err.println("Could not open preveiw window");
        }
    }

    public void startAnalyser(String path) {
        File f = null;
        if (path == null) {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Select patterns folder");
            f = chooser.showDialog(mainStage);
            if (f == null) {
                //TODO Some notification?
                System.exit(0);
            }
        }
        if (f != null) {
            path = f.getAbsolutePath();
        }
        f = new File(path);
        if (f.exists() && f.isDirectory()) {
            File[] patterns = f.listFiles();
            if (patterns.length == 0) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Empty patterns");
                alert.setContentText("Patterns folder is empty");
                alert.setHeaderText(null);
                alert.showAndWait();
            }
        }
        Settings.PATTERN_PATH = path;

        Task<Void> tsk = new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                analyser = null;
                try {
                    analyser = new Analyzer(Settings.PATTERN_PATH);

                    Analyzer.addNotifyResever(new MyNotifyReceiver());
                    while (!Thread.interrupted()) {
                        analyser.analyzeSnapshot();
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }

                } catch (InvalidSnapshotException | InvalidParameterException e) {
                    System.err.println(e.getMessage());
                } catch (Exception e) {
                    System.err.println("Unknown exception!");
                    System.err.println(e.getClass().getName());
                    if (e.getMessage() != null) {
                        System.err.println(e.getMessage());
                    } else {
                        System.err.println("No message given.");
                    }
                    // e.printStackTrace();
                } finally {
                    if (analyser != null) {
                        analyser.destroy();
                    }
                }

                return null;
            }
        };
        Thread t = new Thread(tsk);
        t.start();
        mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                tsk.cancel();
            }
        });

    }

    @FXML
    private void reloadAnalyser() throws InterruptedException {

//        final Alert dialog = new Alert(Alert.AlertType.INFORMATION, "Analyser is restrating. Please wait...");
//        dialog.setHeaderText(null);
//        dialog.getDialogPane().getButtonTypes().clear();
//        dialog.show();
        Task<Void> t = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                analyser.loadPatterns(Settings.PATTERN_PATH);
                return null;
            }
        };
//        t.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
//
//            @Override
//            public void handle(WorkerStateEvent event) {
//               Platform.runLater(new Runnable() {
//
//                   @Override
//                   public void run() {
//                       dialog.close();
//                   }
//               });
//            }
//        });
        Thread th = new Thread(t);
        th.setDaemon(true);
        th.start();
    }

    @FXML
    private void runConfigurator() {
        Stage stage = new Stage(StageStyle.DECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader();
        try {
            BorderPane bp = loader.load(MainViewControler.class.getResourceAsStream("Configurator.fxml"));
            ConfiguratorController controller = loader.getController();
            controller.setStage(stage);
            stage.setScene(new Scene(bp));
            stage.setTitle("Configurator");
            stage.show();
            controller.fillFromSettings();
        } catch (IOException ex) {
            System.err.println("Could not open configurator window");
        }
    }

}
