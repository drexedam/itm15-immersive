package at.itm15.immersive.book;

public enum Algorithm {
	Matching,
	FeatureDetection
}