package at.itm15.immersive.book.notification;

public interface INotifyResever<T> {
	/**
	 * Called when the sender wants to notify the receiver
	 * @param args 
	 */
	public void notifyMe(NotificationArgs<T> args);
}
