package at.itm15.immersive.book.notification;

import at.itm15.immersive.book.cv.AnalyzationResult;

public class MyNotifyReceiver implements INotifyResever<AnalyzationResult> {

	@Override
	public void notifyMe(NotificationArgs<AnalyzationResult> args) {
		AnalyzationResult result = args.getArgs();
		
		if(result.patternFound()) {
			System.out.println("Found a pattern!!");
			System.out.println("For pattern with name: "+result.getName());
		} else {
			System.out.println("No pattern found!!");
		}
		
	}
	
}
