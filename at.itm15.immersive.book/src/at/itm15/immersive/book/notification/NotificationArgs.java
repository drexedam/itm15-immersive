package at.itm15.immersive.book.notification;

/**
 * 
 * 
 * @param <T> The type of argument
 */
public class NotificationArgs<T> {

	public NotificationArgs(T args) {
		this.args = args;
	}
	
	private T args;
	
	public T getArgs() {
		return args;
	}
	
}
