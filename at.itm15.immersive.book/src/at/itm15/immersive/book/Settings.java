package at.itm15.immersive.book;

import at.itm15.immersive.book.debug.DebugWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

public class Settings {
	/**
	 * Loads the settings from ./resources/config.properties
	 */
	public static void init(){
		Properties props = new Properties();
		String propFileName = "config.properties";

		try (InputStream stream = new FileInputStream("./resources/"
				+ propFileName)) {
			try {
				props.load(stream);
			} catch (IOException e) {
				System.out
						.println("Could not load configuration. Using default values.");
			}

			String debugStr = props.getProperty("common.debug");
			String minMatchesStr = props
					.getProperty("featuredetection.minmatches");
			String thresholdStr = props.getProperty("matching.threshold");
			String algorithmStr = props.getProperty("common.algo");
			String cameraIdStr = props.getProperty("common.camid");
                        String patternRepeatStr = props.getProperty("pattern.repeat");

			if (debugStr == null || !debugStr.toLowerCase().equals("true")) {
				DEBUG = false;
			} else {
				DEBUG = true;
			}

			if (algorithmStr != null) {
				switch (algorithmStr) {
				case "matching":
					ALGORITHM = Algorithm.Matching;
					break;
				case "featuredetection":
				default:
					ALGORITHM = Algorithm.FeatureDetection;
					break;
				}
			} else {
				ALGORITHM = Algorithm.FeatureDetection;
				System.out.println("Invalid value for common.algo");
			}
			if (minMatchesStr != null) {
				try {
					MIN_MATCHES = Integer.parseInt(minMatchesStr);
				} catch (Exception e) {
					MIN_MATCHES = 10;
					System.out
							.println("Invalid value for featuredetection.minmatches");
				}
			} else {
				MIN_MATCHES = 10;
				System.out
						.println("Invalid value for featuredetection.minmatches");
			}

			if (thresholdStr != null) {
				try {
					THRESHOLD = Double.parseDouble(thresholdStr);
				} catch (Exception e) {
					THRESHOLD = 0.1;
					System.out.println("Invalid value for matching.threshold");
				}
			} else {
				THRESHOLD = 0.1;
				System.out.println("Invalid value for matching.threshold");
			}

			if (cameraIdStr != null) {
				try {
					CAMERA_ID = Integer.parseInt(cameraIdStr);
				} catch (Exception e) {
					CAMERA_ID = 0;
					System.out.println("Invalid value for common.camid");
				}
			} else {
				CAMERA_ID = 0;
				System.out.println("Invalid value for common.camid");
			}
                        try{
                            PATTERN_REPEAT = Integer.parseUnsignedInt(patternRepeatStr);
                        } catch(NumberFormatException e){
                            System.out.println("Invalid value for pattern repeat. Setting to 4 ");
                            PATTERN_REPEAT = 4;
                        }
                        Enumeration<?> propertyNames = props.propertyNames();
                        while(propertyNames.hasMoreElements()){
                            String key = (String) propertyNames.nextElement();
                            if(key.startsWith("pattern.") && !key.equals("pattern.repeat")){
                                PATTERNS.put(key.substring(8), props.getProperty(key));
                                DebugWriter.println("Pattern Name: " + key.substring(8) + " URL: " + props.getProperty(key));
                            }
                        }
                        
		} catch (IOException e1) {
			System.out
					.println("Could not load configuration. Using default values.");
		}
	}

	public static boolean DEBUG = false;
	public static int MIN_MATCHES = 10;
	public static double THRESHOLD = 0.1;
	public static Algorithm ALGORITHM = Algorithm.FeatureDetection;
	public static int CAMERA_ID = 1;
        public static HashMap<String, String> PATTERNS = new HashMap<>();
        public static int PATTERN_REPEAT;
        public static String PATTERN_PATH;
}
